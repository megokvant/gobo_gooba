<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
</parts>
<sheets>
<sheet>
<plain>
<circle x="36.83" y="36.83" radius="10.77630625" width="0.1524" layer="91"/>
<text x="33.02" y="34.29" size="6.4516" layer="91">M</text>
<text x="1.27" y="44.45" size="1.778" layer="91">BLU</text>
<text x="1.27" y="35.56" size="1.778" layer="91">BLK</text>
<text x="1.27" y="26.67" size="1.778" layer="91">YEL</text>
<text x="25.4" y="3.81" size="1.778" layer="91">GRN</text>
<text x="34.29" y="3.81" size="1.778" layer="91">WHT</text>
<text x="43.18" y="3.81" size="1.778" layer="91">RED</text>
<text x="66.04" y="15.24" size="1.778" layer="91">GRN - A
YEL - B
RED - C
BLUE - D</text>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
<net name="N$2" class="0">
<segment>
<wire x1="36.83" y1="24.13" x2="36.83" y2="6.35" width="0.1524" layer="91"/>
<wire x1="36.83" y1="24.13" x2="38.1" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="38.1" y1="24.13" x2="39.37" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="39.37" y1="24.13" x2="40.64" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="40.64" y1="24.13" x2="41.91" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="41.91" y1="24.13" x2="43.18" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="43.18" y1="24.13" x2="44.45" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="45.72" y1="24.13" x2="45.72" y2="6.35" width="0.1524" layer="91"/>
<wire x1="44.45" y1="24.13" x2="45.72" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="27.94" y1="24.13" x2="29.21" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="29.21" y1="24.13" x2="30.48" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="31.75" y1="24.13" x2="33.02" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="30.48" y1="24.13" x2="31.75" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="33.02" y1="24.13" x2="34.29" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="34.29" y1="24.13" x2="35.56" y2="24.13" width="0.1524" layer="91" curve="-180"/>
<wire x1="35.56" y1="24.13" x2="36.83" y2="24.13" width="0.1524" layer="91" curve="-180"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="27.94" y1="24.13" x2="27.94" y2="6.35" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="24.13" y1="36.83" x2="6.35" y2="36.83" width="0.1524" layer="91"/>
<wire x1="24.13" y1="36.83" x2="24.13" y2="35.56" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="35.56" x2="24.13" y2="34.29" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="34.29" x2="24.13" y2="33.02" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="33.02" x2="24.13" y2="31.75" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="31.75" x2="24.13" y2="30.48" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="30.48" x2="24.13" y2="29.21" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="27.94" x2="6.35" y2="27.94" width="0.1524" layer="91"/>
<wire x1="24.13" y1="29.21" x2="24.13" y2="27.94" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="45.72" x2="24.13" y2="44.45" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="44.45" x2="24.13" y2="43.18" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="41.91" x2="24.13" y2="40.64" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="43.18" x2="24.13" y2="41.91" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="40.64" x2="24.13" y2="39.37" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="39.37" x2="24.13" y2="38.1" width="0.1524" layer="91" curve="-180"/>
<wire x1="24.13" y1="38.1" x2="24.13" y2="36.83" width="0.1524" layer="91" curve="-180"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="24.13" y1="45.72" x2="6.35" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
