<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="5" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
</parts>
<sheets>
<sheet>
<plain>
<circle x="111" y="107" radius="25" width="0.1524" layer="97"/>
<wire x1="51" y1="77" x2="111" y2="77" width="0.1524" layer="97"/>
<wire x1="81" y1="107" x2="81" y2="47" width="0.1524" layer="97"/>
<circle x="81" y="77" radius="5" width="0.1524" layer="97"/>
<circle x="81" y="77" radius="72.111025" width="0.1524" layer="97"/>
<wire x1="131" y1="107" x2="129" y2="109" width="0.1524" layer="97"/>
<wire x1="131" y1="107" x2="133" y2="109" width="0.1524" layer="97"/>
<wire x1="14" y1="117" x2="112" y2="5" width="0.1524" layer="97" curve="-213.690068"/>
<wire x1="14" y1="117" x2="13" y2="120" width="0.1524" layer="97"/>
<wire x1="14" y1="117" x2="17" y2="117" width="0.1524" layer="97"/>
<dimension x1="86.56045625" y1="112.263903125" x2="135.43954375" y2="101.736096875" x3="135.43954375" y3="101.7361" textsize="1.778" layer="91" dtype="diameter"/>
<wire x1="92.7" y1="114.3" x2="131" y2="107" width="0.1524" layer="97" curve="-162.16455"/>
<circle x="111" y="47" radius="25" width="0.1524" layer="97"/>
<wire x1="131" y1="47" x2="129" y2="49" width="0.1524" layer="97"/>
<wire x1="131" y1="47" x2="133" y2="49" width="0.1524" layer="97"/>
<dimension x1="86.56045625" y1="52.263903125" x2="135.43954375" y2="41.736096875" x3="111" y3="47.000003125" textsize="1.778" layer="91" dtype="diameter"/>
<wire x1="92.7" y1="54.3" x2="131" y2="47" width="0.1524" layer="97" curve="-162.16455"/>
<circle x="51" y="47" radius="25" width="0.1524" layer="97"/>
<wire x1="71" y1="47" x2="69" y2="49" width="0.1524" layer="97"/>
<wire x1="71" y1="47" x2="73" y2="49" width="0.1524" layer="97"/>
<dimension x1="26.56045625" y1="52.263903125" x2="75.43954375" y2="41.736096875" x3="51" y3="47.000003125" textsize="1.778" layer="91" dtype="diameter"/>
<wire x1="32.7" y1="54.3" x2="71" y2="47" width="0.1524" layer="97" curve="-162.16455"/>
<circle x="51" y="107" radius="25" width="0.1524" layer="97"/>
<wire x1="71" y1="107" x2="69" y2="109" width="0.1524" layer="97"/>
<wire x1="71" y1="107" x2="73" y2="109" width="0.1524" layer="97"/>
<dimension x1="26.56045625" y1="112.263903125" x2="75.43954375" y2="101.736096875" x3="51" y3="107.000003125" textsize="1.778" layer="91" dtype="diameter"/>
<wire x1="32.7" y1="114.3" x2="71" y2="107" width="0.1524" layer="97" curve="-162.16455"/>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
